//
// Processor 12F629 12F635
//

#define _XTAL_FREQ 14318180.0

#include <htc.h>

// high speed crystal
// do not protect memory
// brown-out reset disabled
// MCLR pin function disabled
// power-up timer enabled
// watchdog timer disabled

__CONFIG(HS & UNPROTECT & BORDIS & MCLREN & PWRTEN & WDTDIS & 0x31FF);

#include "global.h"

char state = SLEEPING;
char pwm_step = 0;
char dimm_direction = DIMM_UP;

char validate_counter;
  

void pwm(char step)
{
  if (step > PWM_STEP_MAX) 
  {
    pwm_on = pwm_duty[PWM_STEP_MAX];
  }    
  else
  {
    pwm_on = pwm_duty[step];
  }
}  

// start - dimmer value at start 
//         DIMM_MAX - maximal lighting
//         DIMM_MIN - dark
void init_dimm(unsigned short start)
{
  if (start == DIMM_MIN)
  {
    dimm_direction = DIMM_UP;
    pwm_step = PWM_STEP_MIN;
  }
  else
  {
    dimm_direction = DIMM_DOWN;
    pwm_step = PWM_STEP_MAX;
  }  
  pwm(pwm_step);
  t_dimm = 0;
}
  
void dimm(void)
{
  if (dimm_direction == DIMM_UP)
  {
    ++pwm_step;
  }
  else
  {
    --pwm_step;
  }    
  
  if (pwm_step <= PWM_STEP_MIN)
  {
    dimm_direction = DIMM_UP;
  }
  else if (pwm_step >= PWM_STEP_MAX)
  {
    dimm_direction = DIMM_DOWN;
  }
      
  pwm(pwm_step);
}

void main(void)
{
  WPUDA = 0x00;         // but disable all pull-ups

  OPTION = OPTION_CONFIG; 
  
  CMCON0 = 0x07;        // Digital I/O
  GPIO = 0;
  
	TRISIO = 0b11111100;	// GP<1:0> output	
  
  TMR0 = TMR0_LOAD;
    
  state = SLEEPING;

  // main loop	
  for(;;)
  {
    switch(state)
    {
      case SLEEPING: 
      {
        POWER_OFF;

        INTCON = 0x00;  // Disable all interrupts and clear flags
        LED_OFF;

        INTEDG = 1;     // rising edge trigger interrupt
        INTE   = 1;     // Enable GPIO2 edge-triggered interrupt
        
        SLEEP();
        
        validate_counter = 0;
        state = VALIDATE_WAKEUP;
        break;
      }
      // glitch filter - checks the button x times for the next x*50 ms, 
      // if the button is not pressed during this periode the wakeup appears because of a glitch and we can lay down again
      case VALIDATE_WAKEUP:           
      {
      	__delay_ms(50);
      	if (GP2)
      	{
        	++validate_counter;
        	if (validate_counter == 4)
        	{
          	state = WAKEUP;
          } 	
        }
        else
        {
          state = SLEEPING;
        }
        break;
      }
      case WAKEUP:
      {            
        init_dimm(DIMM_MAX);
        POWER_ON;
        
        INTE   = 0;     // Disable GPIO2 interrupt
        INTF   = 0;     // Reset GPIO2 flag
        
        t_press = 0;
        T0IF = 0;       // Reset TMR0 interrupt flag
        T0IE = 1;       // Enable TMR0 interrupt
        GIE  = 1;       // Enable Global interrupt
        
        while (t_press < 10)   // wait to determine BUTTON_PRESSED
        {
          asm("nop");
        }  
        
        t_press = 0;
        state = ACTIVATE;

        break;
      }  
      case ACTIVATE:   
      {
        if (BUTTON_PRESSED)
        {
          if (t_press >= TIME_ACTIVATE_DIMMER)
          {
            t_dimm = 0;
            state = DIMMING;
          }
        }
        else
        {
          if (t_press >= TIME_ACTIVATE_RED_ALERT)
          {
            state = RED_ALERT;
          }
          else
          {
            state = HOLD;
          }
        }    
        break;
      }
      case HOLD: 
      {
        if (BUTTON_PRESSED)
        {
          t_press = 0;
          state = DEACTIVATE;
        }  
        break;
      }
      case RED_ALERT:
      {
        if (BUTTON_PRESSED)
        {
          state = HOLD;
        }
        else
        {  
          if (t_dimm >= TIME_RED_ALERT_STEP)
          {
            dimm();
            t_dimm = 0;
          }    
        }
        break;
      }
      case DIMMING:   
      {
        if (BUTTON_PRESSED)
        {
          if (t_dimm >= TIME_DIMMER_STEP)
          {
            dimm();
            t_dimm = 0;
          }    
        }
        else
        {
          state = HOLD;
        }    
        break;
      }
      case DEACTIVATE:  
      {
        if (BUTTON_PRESSED)
        {
          if (t_press >= TIME_ACTIVATE_DIMMER)
          {
            t_dimm = 0;
            state = DIMMING;
          }  
        }
        else
        {
          state = SLEEPING;
        }    
        break;
      }
    }  
  }
}
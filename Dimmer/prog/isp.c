// interrupt service program
#include <htc.h>
#include "global.h"

void interrupt my_isr(void)
{
  // only Timer0 Interrupt enabled
  T0IF = 0;   // clear interrupt flag
  TMR0 = TMR0_LOAD;

  ++isr_t_pwm;
  
  if ((isr_t_pwm & 0x3F) == 0x20)
  {
    ++t_press;
  }
    
  if ((isr_t_pwm & 0x07) == 0x04)
  {  
    ++t_dimm; 
    button_queue = (button_queue << 1);
    button_queue |= GP2;
  }

  if (isr_t_pwm <= pwm_on)
  {
    LED_ON;
  }
  else
  {
    LED_OFF;
  }
  
}


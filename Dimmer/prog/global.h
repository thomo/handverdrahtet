// globals

#ifndef GLOBAL_H
#define GLOBAL_H

#define bitset(var,bitno) ((var) |= (1 << (bitno)))
#define bitclr(var,bitno) ((var) &= ~(1 << (bitno)))
#define bittst(var,bitno) (var & (1 << (bitno)))

#define LOWBYTE(v)   ((unsigned char) (v))
#define HIGHBYTE(v)  ((unsigned char) (((unsigned int) (v)) >> 8))

#define PIN_POWER   0
#define PIN_LED     1

                      //0-------  global disable weak pull-ups
                      //-x------
                      //--0-----  TMR0 Source is internal clock
                      //---x----
                      //----0---  PreScaler to TMR0
                      //-----001  PreScaler = 4
#define OPTION_CONFIG 0b10000001

volatile char button_queue;

#define BUTTON_PRESSED button_queue == 0xFF

#if	defined(_12F629)
#define GP0 GPIO0
#define GP1 GPIO1
#define GP2 GPIO2
#define WPUDA WPU
#define CMCON0 CMCON
#endif

#define POWER_ON  GP0 = 1
#define POWER_OFF GP0 = 0
#define LED_ON    GP1 = 1
#define LED_OFF   GP1 = 0  

// FSM states
#define SLEEPING  0  
#define DEACTIVATE    1
#define ACTIVATE    2
#define DIMMING   3
#define HOLD  4
#define VALIDATE_WAKEUP 5
#define WAKEUP 6
#define RED_ALERT 7

// PWM_STEP_XXX  - Number of PWM steps
#define PWM_STEP_MIN 0
#define PWM_STEP_MAX 99

#define DIMM_UP   1
#define DIMM_DOWN 0

#define DIMM_MAX  1
#define DIMM_MIN  0

#define TIME_ACTIVATE_DIMMER 0xF0
#define TIME_DIMMER_STEP 0x80
#define TIME_ACTIVATE_RED_ALERT 0x70
#define TIME_RED_ALERT_STEP 0x30

#define TMR0_LOAD 200


volatile char pwm_on;

volatile char t_press;
volatile char t_dimm;

char isr_t_pwm;
char isr_status;

const char pwm_duty[PWM_STEP_MAX +1] = {
2,
3,
3,
4,
4,
5,
5,
6,
7,
8,
9,
9,
10,
11,
12,
13,
15,
16,
17,
18,
19,
21,
22,
23,
25,
26,
28,
29,
31,
33,
34,
36,
38,
40,
42,
43,
45,
47,
49,
52,
54,
56,
58,
60,
63,
65,
67,
70,
72,
75,
77,
80,
83,
85,
88,
91,
93,
96,
99,
102,
105,
108,
111,
114,
118,
121,
124,
127,
131,
134,
137,
141,
144,
148,
151,
155,
159,
162,
166,
170,
174,
178,
182,
186,
190,
194,
198,
202,
206,
210,
215,
219,
223,
228,
232,
237,
241,
246,
250,
255
};

#endif